//diez primeras potencias de 2
#include <stdio.h>
#include <math.h>
int main(int argc, char const *argv[]) {
  int i;
  for( i = 0; i < 10; i++) {
    printf("%f. \n", pow(i,2));
  }
  return 0;
}
