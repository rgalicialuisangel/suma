//tabla del 3
#include <stdio.h>
int main(int argc, char const *argv[]) {
  int i,c,s;

  do {
    printf("ingrese el limite inferior de la tabla\n" );
    scanf("%i",&c);
    printf("ingrese el limite superior  de la tabla\n" );
    scanf("%i",&s);
    if (c>s) {
      printf("el limite inferior de la tabla debe ser menor al limite superior de la tabla\n" );
    }
  } while(c>s);
  for( i = c; i <= s; i++) {
    printf("3 x %i = %i \n",i,i*3 );
  }
  return 0;
}
