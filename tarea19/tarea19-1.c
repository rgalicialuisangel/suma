#include <stdio.h>
#include <stdlib.h>
#include "info.h"
int main(int argc, char const *argv[]) {
  appInfoData("tipos y direcciones de memoria","13/11/2018");
  char letra='a';
  unsigned char  letraExtendida='a';
  short enteroCorto=10;
  int entero=30123;
  unsigned int enteroLargo=2345898898;
  float decimal=11.4f;
  double decimalDoblePrec=12.9989d;
  printf("Valor ; %c direccion : %p tamaño :%lu  byte (s)\n",letra,&letra,sizeof(letra) );
  printf("Valor ; %c direccion : %p tamaño :%lu  byte (s)\n",letraExtendida,&letraExtendida,sizeof(letraExtendida) );
    return 0;
}
